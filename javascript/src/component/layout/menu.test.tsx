import React from 'react';
import Menu from './menu';
import {render} from '@testing-library/react'
import { getPages } from '../../services/page';
jest.mock('../../services/page');

const fakePages = [{label:'home', path:'/'}, {label:'account', path:'/account'}]

beforeEach(() => {
    //@ts-ignore
    getPages.mockReturnValue(fakePages);
});

test('menu to load', ()=> {
  const menu = render(<Menu />);
  expect(menu.baseElement).not.toBeNull();
});

test('menu has all the links from pages.getPages', async () =>{
  const menu = render(<Menu />);
  const links = await menu.findAllByTestId('page');
  const linkText = links.map(link => link.textContent);
  const expectedLabels = fakePages.map(page => page.label);
  expect(linkText).toEqual(expectedLabels);
})
