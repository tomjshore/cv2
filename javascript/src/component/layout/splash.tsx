import React from 'react';
import myPic from '../../img/me480.jpg';

export default class Splash extends React.Component {

  render(){
    return (<div className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-three-fifths is-offset-one-fifth">
          
            <div className="card">
              <div className="card-content">
                <div className="media">
                  <div className="media-left">
                    <figure className="image is-128x128">
                      <img src={myPic} alt='tomjshore face' className="is-rounded"/>
                    </figure>
                  </div>
                  <div className="media-content">
                    <p className="title is-4">Tom Shore</p>
                    <p className="subtitle is-6"><a href="mailto:tom@tomjshore.co.uk">tom@tomjshore.co.uk</a></p>
                  </div>
                </div>
                <div className="content">
                  <p>Lead Java Developer, Scrum Master who would be looking for opportunities to work in agile teams that wish to improve the way that work by making there working environment more fun, driven and focused on striving towards quality. Would easily fit in to team that are working with FileNet as I have good experience in that area.</p>      
                </div>
              </div>
            </div>

            <br/>
            <div className="columns">
              <div className="column is-three-fifths is-offset-one-fifth has-text-centered">
                <a href="/CV-master.pdf" className="button is-large is-primary">Download CV</a>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>);
  }
}
