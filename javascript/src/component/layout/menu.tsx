import React from 'react';
import { getPages, Page } from '../../services/page';


export default class Menu extends React.Component {

  private pages : Page[];

  constructor(props : any){
      super(props);
      this.pages = getPages();
  }

  render(){
    return (
        <nav className="navbar is-dark" role="navigation">
          <div className="navbar-brand">
            <a className="navbar-item" href="/">
              tomjshore
            </a>
          </div> 
          <div className="navbar-end">           
            {this.renderLinks()}
          </div>       
        </nav>)
  }

  private renderLinks() : JSX.Element[] {
    return this.pages.map(page => {
        return (
          <a href={page.path} className='navbar-item' data-testid="page" key={page.label}>{page.label}</a>
        );
    });
  }

}
