import React from 'react';
import App from './App';
import {render} from '@testing-library/react'


test('The App loads', () => {
  const  app = render(<App />);
  expect(app).not.toBeNull();
});
