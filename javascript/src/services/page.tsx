
function getPages (): Page[] {
  return [new Page('home', '/')]
}


class Page {
  private _label : string
  private _path : string

  constructor(label: string, path :string){
    this._label = label;
    this._path = path;
  }

  public get label() : string {
    return this._label;
  }

  public get path() : string {
    return this._path;
  }
}

export {getPages, Page}
