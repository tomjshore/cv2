import React from 'react';
import Menu from './component/layout/menu';
import Splash from './component/layout/splash';

const App = () => {
  return (
  <>
  <Menu/>
  <Splash />
</>
  );
}

export default App;
