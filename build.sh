#!/bin/bash

mvn clean

mkdir -p src/main/resources/react
mkdir -p src/main/resources/static/static/media

rm src/main/resources/react/**
rm src/main/resources/static/static/media/**

cd javascript
CI=true npm test
npm run build

cp build/static/js/**.js ../src/main/resources/react/
cp build/static/css/**.css ../src/main/resources/react/
cp build/static/media/**.svg ../src/main/resources/static/static/media/
cp build/static/media/**.jpg ../src/main/resources/static/static/media/

cd ..
mvn package



