package com.tomjshore.cv2.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ReactServiceTest {

	private static final String RUNTIME_CONTENT = "runtime content";
	private static final String SECOND_CONTENT = "2nd content";
	private static final String MAIN_CONTENT = "main content";
	private static final String CSS_CONTENT = "css content";

	@InjectMocks
	private ReactService reactService;

	@Mock
	private PathMatchingResourcePatternResolver resourceLoader;

	@Mock
	private Resource runtimeResource;

	@Mock
	private Resource secondResource;

	@Mock
	private Resource mainResource;

	@Mock
	private Resource cssResource;


	@BeforeEach
	public void setUp() throws Exception{
		Resource[] resources = {runtimeResource, secondResource, mainResource, cssResource};
		given(resourceLoader.getResources("classpath:react/*")).willReturn(resources);

		given(runtimeResource.getInputStream()).willReturn(IOUtils.toInputStream(RUNTIME_CONTENT,StandardCharsets.UTF_8));
		given(secondResource.getInputStream()).willReturn(IOUtils.toInputStream(SECOND_CONTENT,StandardCharsets.UTF_8));
		given(mainResource.getInputStream()).willReturn(IOUtils.toInputStream(MAIN_CONTENT,StandardCharsets.UTF_8));
		given(cssResource.getInputStream()).willReturn(IOUtils.toInputStream(CSS_CONTENT,StandardCharsets.UTF_8));

		given(runtimeResource.getFilename()).willReturn("runtime-main.123.js");
		given(secondResource.getFilename()).willReturn("2.123.chunk.js");
		given(mainResource.getFilename()).willReturn("main.123.chunk.js");
		given(cssResource.getFilename()).willReturn("main.123.chunk.css");

	}


	@Test
	public void testGetStartUpResources() throws IOException{
		//given

		//when
		Map<String, String> resources = reactService.getStartUpResources();

		//then
		assertEquals(RUNTIME_CONTENT, resources.get(ReactService.REACT_RUNTIME));
		assertEquals(SECOND_CONTENT, resources.get(ReactService.SECOND_CHUNK));
		assertEquals(MAIN_CONTENT, resources.get(ReactService.MAIN_CHUNK));
		assertEquals(CSS_CONTENT, resources.get(ReactService.CSS));		
	}

	@Test
	public void testGetStartUpResourceThrowsWhenNoClasspath() throws IOException {
		//given
		doThrow(new IOException()).when(resourceLoader).getResources("classpath:react/*");
		//when
		assertThrows(IOException.class, () -> reactService.getStartUpResources());

		//then
	}

}
