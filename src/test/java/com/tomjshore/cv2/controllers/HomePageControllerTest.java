package com.tomjshore.cv2.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.ModelAndView;

import com.tomjshore.cv2.services.ReactService;

import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class HomePageControllerTest {
	
	@Mock
	private ReactService reactService;
	
	@InjectMocks
	private HomePageController controller;
	
	
	@BeforeEach
	public void setUp() throws IOException {
		given(reactService.getStartUpResources()).willReturn(Map.of("1","1"));
	}
	
	@Test
	public void testHomePageReturnsTheIndexTemplate() throws Exception {
		//given
		
		//when
		ModelAndView modelAndView = controller.displayHomePage();
		
		//then
		assertEquals("index", modelAndView.getViewName());
	}
	
	@Test
	public void testHomePageReturnsReactRuntimeInModel() throws Exception {
		//given
		
		//when
		ModelAndView modelAndView = controller.displayHomePage();

		//then
		Object runtimeObject = modelAndView.getModel().get("1");
		assertTrue(runtimeObject instanceof String);
		String runtime = (String)runtimeObject; 
		assertEquals("1", runtime);
	}

}
