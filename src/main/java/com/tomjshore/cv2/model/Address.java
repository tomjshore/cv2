package com.tomjshore.cv2.model;

public class Address {
	
	private String line1;
	private String line2;
	private String line3;
	private String postCode;
	
	public Address(String line1, String line2, String line3, String postCode) {
		this.line1 = line1;
		this.line2 = line2;
		this.line3 = line3;
		this.postCode = postCode;
	}
	
	@Override
	public String toString() {
		return String.format("%s, %s, %s %s", line1, line2, line3, postCode);
	}

}
