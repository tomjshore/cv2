package com.tomjshore.cv2.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PagesConst.ADMIN)
public class AdminController {
	
	@GetMapping
	public String test() {
		return "admin";
	}

}
