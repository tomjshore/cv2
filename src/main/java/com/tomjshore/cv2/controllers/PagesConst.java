package com.tomjshore.cv2.controllers;

public class PagesConst {
	
	private static final String API_BASE = "/api";
	
	public static final String HOME = "/";
	public static final String ADMIN = API_BASE + "/admin";

}
