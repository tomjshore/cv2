package com.tomjshore.cv2.controllers;


import java.io.IOException;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tomjshore.cv2.services.ReactService;

@Controller
@RequestMapping(PagesConst.HOME)
public class HomePageController {
	
	private ReactService reactService;

	
	public HomePageController(ReactService reactService) {
		this.reactService = reactService;
	    
	}

	@GetMapping
	public ModelAndView displayHomePage() throws IOException {

		return new ModelAndView("index", reactService.getStartUpResources());
	}
	
	
}
