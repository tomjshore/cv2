package com.tomjshore.cv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@SpringBootApplication
@Configuration
public class Cv2Application {

	public static void main(String[] args) {
		SpringApplication.run(Cv2Application.class, args);
	}

	@Bean
	public PathMatchingResourcePatternResolver getPathMatchingResourcePatternResolver() {
		return new PathMatchingResourcePatternResolver();
	}
}

