package com.tomjshore.cv2.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;



@Service
public class ReactService {

	private final static Logger LOG = LoggerFactory.getLogger(ReactService.class);
	
	private final PathMatchingResourcePatternResolver resourceLoader;
	
	public static final String REACT_RUNTIME = "react_runtime";
	public static final String SECOND_CHUNK = "react_2nd";
	public static final String MAIN_CHUNK = "react_main";
	public static final String CSS = "react_css";
	
	private boolean loaded = false;
	private Map<String,String> resourcesMap;

	
	private ReactService(PathMatchingResourcePatternResolver resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	
	public Map<String, String>getStartUpResources() throws IOException{
		if(!this.loaded) {
			loadFiles();
		}

		return resourcesMap;
	}

	private void loadFiles() throws IOException{
		
		LOG.info("Loading react resources from classpath");
		Resource[] resources = resourceLoader.getResources("classpath:react/*");
		resourcesMap = new HashMap<>();


		findFileFromRegex(resources, "runtime.*\\.js")
				.ifPresent(resource -> addToResources(resourcesMap, REACT_RUNTIME, resource));

		findFileFromRegex(resources, "2.*chunk\\.js")
				.ifPresent(resource -> addToResources(resourcesMap, SECOND_CHUNK, resource));

		findFileFromRegex(resources, "main.*\\.js")
				.ifPresent(resource -> addToResources(resourcesMap, MAIN_CHUNK, resource));

		findFileFromRegex(resources, "main.*\\.css")
				.ifPresent(resource -> addToResources(resourcesMap, CSS, resource));


	    this.loaded = true;
	}
	
	private Optional<Resource> findFileFromRegex(Resource[] resouces, String regex) {
		Pattern pattern = Pattern.compile(regex);
		return Arrays.stream(resouces).filter(file ->
			 pattern.matcher(Objects.requireNonNull(file.getFilename())).matches()
		).findAny();
	}
	
	private void addToResources(Map<String, String> resources, String key, Resource resource) {
			try(InputStream stream = resource.getInputStream()) {
				resources.put(key, IOUtils.toString(stream, StandardCharsets.UTF_8));
			}catch (IOException e){
				LOG.error("Unable to load resource", e);
				resources.put(key, "");
			}
	}
}
